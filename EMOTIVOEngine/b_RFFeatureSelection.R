form.in <- paste(target, ".", sep="~")
results <- randomForest(as.formula(form.in),data = rawData) ## Random Forest based on test data    
additionalFeatures <- c("language")
featuresExcludedFromFormula <- c(target)
targetValues <- rawData[target]
additionalValues <- rawData[additionalFeatures]
# rawData3 <- rawData[,sort(results$importance, decreasing = TRUE, index.return = TRUE)]
# rawData <- cbind(rawData,additionalValues)
# rawData <- cbind(rawData,targetValues)

# columnNames <- colnames(rawData)
# totalFeatures <- length(columnNames)
# numFeatures <- totalFeatures - length(featuresExcludedFromFormula)
# form.in <- paste(target," ~ ", columnNames[1],sep="")
# for(feature in columnNames[2:numFeatures])
# {
#   if(!(feature  %in% featuresExcludedFromFormula))
#   {
#     form.in <- paste(form.in,feature,sep = "+")
#   }
# }
