if( "parentclip" %in% colnames(rawData))
{
  datasize <- length(unique(rawData$parentclip))
} else 
{
  datasize <- nrow(rawData)
}

index <- 1:datasize
index <- sample(datasize) ### shuffle index
fold <- rep(1:kfolds, each=datasize/kfolds)[1:datasize]
folds <- split(index, fold) ### create list with indices for each fold

testDataMultiClassAll <- data.frame()


for(j in 1:kfolds)
{
  cat(j,"...")
  if(datasize == nrow(rawData))
  {
    trainData <- rawData[-folds[[j]],] ### training set is all data except for fold
    testData <- rawData[folds[[j]],]
    actualdata <- rawData[folds[[j]], target] ### emotions of the test set
    
  }else {
    trainData <- rawData[!(rawData$parentclip %in% folds[[j]]),] ### training set is all data except for fold group by parent
    testData <- rawData[rawData$parentclip %in% folds[[j]],]
    actualdata <- rawData[rawData$parentclip %in% folds[[j]], target] ### emotions of the test set
    
  }
  
  source(file=paste(PATH,machineLearningModel,".R",sep=""))  ### non-esemble method
  testDataMultiClassAll <- rbind(testDataMultiClassAll,testDataMultiClass)
  levels(actualdata) <-factorlevels
}
