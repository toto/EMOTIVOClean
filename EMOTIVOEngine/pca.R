library(caret)

trans = preProcess(rawData[,1:(ncol(rawData)-2)],
                     method=c("BoxCox", "center",
                             "scale", "pca"))
#PC = prcomp(rawData[,1:(ncol(rawData)-2)])[[2]][,1:numberOfFeatures]
PC = predict(trans, rawData[,1:(ncol(rawData)-2)])
cfsSubsetOfFeatures <- colnames(PC)
PC$emotion <- rawData[,(ncol(rawData)-1)]
PC$language <- rawData[,ncol(rawData)]

rawData <- PC

targetValues <- rawData[target]
additionalValues <- rawData[additionalFeatures]

columnNames <- colnames(rawData)
totalFeatures <- length(columnNames)
numFeatures <- totalFeatures - length(featuresExcludedFromFormula)
form.in <- paste(target," ~ ", columnNames[1],sep="")
selectedFeaturesArray <- c()
for(feature in columnNames[2:numFeatures])
{
  if(!(feature  %in% featuresExcludedFromFormula))
  {
    form.in <- paste(form.in,feature,sep = "+")
    selectedFeaturesArray <- c(selectedFeaturesArray,feature)
    
  }
}
