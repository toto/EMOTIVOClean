git clone http://solar-10.wpi.edu/toto/EMOTIVO.git
install libtool (apt-get install libtool)
re-compile the included openSmile: buildStandalone.sh
install R (instructions on the R site)
install libraries:

install.packages("kknn")
install.packages("c50")
install.packages("randomForest")
install.packages("e1071")
install.packages("neuralnet)
install.packages("dplyr")
install.packages("e1071")
install.packages("Matrix")
install.packages("nnet")

Run code

#!/bin/bash
#Test: shallowtest, deeptest, fulltest
# ./runexperiments.sh -cutstart 1 -cutstep 1  -cutend 9 -overlapstart 0 -overlapstep 0.05 -overlapend 0.96 -runmlalgorithms TRUE -destroytemp TRUE -runtype shallowtest -numthreads 4
#Real Run
# ./runexperiments.sh -cutstart 2 -cutstep 1  -cutend 10 -overlapstart 0 -overlapstep 0.2 -overlapend 0.81 -runmlalgorithms TRUE -destroytemp TRUE -runtype truerun -numthreads 6

NOTE: Currently code runs well in MacOSX some data cross contamination when using more then 1 thread are observed during cleaning in Linux Distros, we are working to fix this bugs.
Please use a Posix Unix Environment in mean time (until this message is removed from readme)