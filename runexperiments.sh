#!/bin/bash
# ./runexperiments.sh -cutstart 1 -cutstep 1  -cutend 9 -overlapstart 0 -overlapstep 0.05 -overlapend 0.96 -runmlalgorithms TRUE -destroytemp TRUE -runtype shallowtest -numthreads 4
#TEst
# ./runexperiments.sh -cutstart 2 -cutstep 1  -cutend 10 -overlapstart 0 -overlapstep 0.2 -overlapend 0.81 -runmlalgorithms TRUE -destroytemp TRUE -runtype truerun -numthreads 6


#Argument Parsing
while [ "$1" ]; do
  case $1 in
    -cutstart)
      shift
      cutstart=$1
      ;;
    -cutend)
      shift
      cutend=$1
      ;;
    -cutstep)
      shift
      cutstep=$1
      ;;  
    -overlapstart)
      shift
      overlapstart=$1
      ;;
    -overlapend)
      shift
      overlapend=$1
      ;;
    -overlapstep)
      shift
      overlapstep=$1
      ;;
    -runmlalgorithms)
      shift
      runmlalgorithms=$1
      ;;        
    -destroytemp)
      shift
      destroytemp=$1
      ;;  
    -runtype)
	  shift
	  runtype=$1
      ;;  
    -numthreads)
    shift
    numthreads=$1
      ;;   
    *)
      echo "Invalid option: -$1" >&2
      exit 1
      ;;
    :)
  esac
  shift
done
let iter="0"

for cuts in $(seq $cutstart $cutstep $cutend)
do
	# Overlap iterations
	for overlap in $(seq $overlapstart $overlapstep $overlapend)
	do
     let iter="$iter+3"
     echo Iteration: $iter Threads: $numthreads + 2 
     if [[ $iter -gt $numthreads ]]; then # This line ensures i'm only processing numthreads+2 jobs at a time.
         echo "Waiting .. "; 
         wait; 
         echo "Done"; 
         let iter="0"
     fi
    #./run.sh WavRepository NumberOfCuts PercentOverlap CurrentTimestamp FilterToExtractEmotionFromFilename TRUE:RunRScripts/FALSE:PreProcessOnly TRUE:DeleteTempFiles/FALSE:KeepTempFiles
  	if [ $runtype = "shallowtest" ]; then
    	# Shallow TEST
  		# Rayerson EmoDB cleanup filter: FindandReplaceCSV.R
  		echo ./run.sh EmoDbWav $cuts $overlap $(date +"%s") CleanRyersonEmoDb.R $runmlalgorithms $destroytemp &
  		# SAVEE cleanup filter: FindandReplaceSAVEECSV.R
  		echo ./run.sh SAVEE $cuts $overlap $(date +"%s") CleanSAVEE.R $runmlalgorithms $destroytemp &
  		# EMODB Berlin cleanup filter: FindandReplaceBerlinCSV.R # 
  		echo ./run.sh BerlinEmoDB $cuts $overlap $(date +"%s") CleanBerlinEmoDb.R $runmlalgorithms $destroytemp &
  	elif [ $runtype = "deeptest" ]; then
    		# Deeper test
  		# Rayerson EmoDB cleanup filter: FindandReplaceCSV.R
  		./run.sh EmoDbWav $cuts $overlap $(date +"%s") CleanRyersonEmoDb.R $runmlalgorithms $destroytemp echo &
  		# SAVEE cleanup filter: FindandReplaceSAVEECSV.R
  		./run.sh SAVEE $cuts $overlap $(date +"%s") CleanSAVEE.R TRUE TRUE echo &
  		#EMODB Berlin cleanup filter: FindandReplaceBerlinCSV.R 
  		./run.sh BerlinEmoDB $cuts $overlap $(date +"%s") CleanBerlinEmoDb.R $runmlalgorithms $destroytemp echo &
	  elif [ $runtype = "fulltest" ]; then
  		# Actual Test
  		# Rayerson EmoDB Sample cleanup filter: FindandReplaceCSV.R
  		./run.sh SampleWavs $cuts $overlap $(date +"%s") CleanRyersonEmoDb.R $runmlalgorithms $destroytemp 
	  elif [ $runtype = "truerun" ]; then
  		# Rayerson EmoDB cleanup filter: FindandReplaceCSV.R
  		./run.sh EmoDbWav $cuts $overlap $(date +"%s") CleanRyersonEmoDb.R $runmlalgorithms $destroytemp &
  		# SAVEE cleanup filter: FindandReplaceSAVEECSV.R
  		./run.sh SAVEEFULL $cuts $overlap $(date +"%s") CleanSAVEE.R $runmlalgorithms $destroytemp &
  		# EMODB Berlin cleanup filter: FindandReplaceBerlinCSV.R # 
  		./run.sh BerlinEmoDB $cuts $overlap $(date +"%s") CleanBerlinEmoDb.R $runmlalgorithms $destroytemp & 
  	  else
  		echo Invalid run type, valid runs are = "shallowtest", "deeptest", "fulltest", "truerun".
  		exit 1
	  fi
	done
done
