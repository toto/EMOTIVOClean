#!/usr/bin/python
import sys
import csv

with open(str(sys.argv[1]), 'rb') as f:
    reader = csv.reader(f, delimiter=';')
    header = next(reader)
    data_list = list(reader)
    rows = ['{:.1f}'.format(sum(float(x) for x in y) / len(data_list)) for y in zip(*data_list)[:]]
    average_data_list = ','.join(map(str, rows)) 
    print average_data_list