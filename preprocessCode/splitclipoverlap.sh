#!/bin/bash
echo Splitting $1/$2 in $3 parts of with $4 overlap into temporary folder $5

# Extract clip length from wav file
L=$(sox $1/$2 -n stat 2>&1 | sed -n 's#^Length (seconds):[^0-9]*\([0-9.]*\)$#\1#p')
echo Length is $L
# Clip length x is x = L/(n-(n-1)k)
x=$(echo $3-1 | bc -l)
x=$(echo $x*$4 | bc -l)
x=$(echo $3-$x | bc -l)
x=$(echo $L/$x | bc -l)
for i in $(seq 1 $3);
do
	if [ $i = 1 ]; then
		#CLIP START
    	S=$(echo 0 | bc -l)
    	#CLIP END
    	E=$(echo $S+$x | bc -l)      
    else
		#CLIP START
    	S=$(echo $E-$4*$x | bc -l)
    	#CLIP END
    	E=$(echo $S+$x | bc -l)    
	fi
	sox $1/$2 ./temp/WAV$6/$2$(echo $i)Clip.wav trim $S $E
done    
