#!/bin/bash
find ./$1 -name '*.'$2 -exec sh -c 'ffmpeg -i $0 -acodec pcm_s16le -ac 2 "./WAVoutput/$(basename $(dirname $(dirname $0)))_$(basename $(dirname $0))_$(basename $0).wav"' {} \;

