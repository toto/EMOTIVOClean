#!/bin/bash
length=$(sox $1 -n stat 2>&1 | sed -n 's#^Length (seconds):[^0-9]*\([0-9.]*\)$#\1#p')
track=$(echo $length/$2 | bc -l)
twotrack=$(echo $track*2 | bc -l)
echo $track
sox $1 $1Clip1.wav trim 0 $track
echo $twotrack
sox $1 $1Clip2.wav trim $track $twotrack
echo $length
sox $1 $1Clip3.wav trim $twotrack $length
