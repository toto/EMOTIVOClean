#!/bin/bash
FILES=/datastorage/EMOTIVOClean/Data/*
for f in $FILES
do
  echo "Processing $f file..."
  # take action on each file. $f store current file name
  R CMD BATCH --no-timing --no-save --no-restore '--args '"${f##*/}"'' ./EMOTIVOEngine/EMOTIVO.R  ./temp/RTrailEMOTIVO${f##*/}.out &
  if (( $(wc -w <<<$(jobs -p)) % 10 == 0 )); then wait; fi

done

