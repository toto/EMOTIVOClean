#!/bin/bash
for file in ./Data/*$1Parts*.csv; do
	nohup R CMD BATCH --no-timing --no-save --no-restore '--args '"${file##*/}"'' ./EMOTIVOEngine/EMOTIVO.R  ./temp/RTrailEMOTIVO${file##*/}.out & 
done
