#!/bin/bash
echo Environment Setup ...
$8 mkdir ./temp/CSV$1_$2_$3_$4

if [ $2 -gt 1 ]; then
$8 	mkdir ./temp/WAV$1_$2_$3_$4
	echo Spliting Clips ...
	for file in $1/*.wav; do
		$8 ./preprocessCode/splitclipoverlap.sh $1 ${file##*/} $2 $3 ./temp/WAV$1_$2_$3_$4 $1_$2_$3_$4
	done
	echo Feature Extraction ...
	for file in ./temp/WAV$1_$2_$3_$4/*.wav; do
		$8 ./openSMILE-2.1.0/SMILExtract -C ~/EMOTIVO/openSMILE-2.1.0/config/emobase2010.conf -I ./temp/WAV$1_$2_$3_$4/${file##*/} -O  "./temp/CSV$1_$2_$3_$4/${file##*/}.csv"
	done
	if [ $7 = "TRUE" ]; then
	$8	rm -R -f ./temp/WAV$1_$2_$3_$4
	fi
else
	echo Feature Extraction ...
	for file in ./$1/*.wav; do
		$8 ./openSMILE-2.1.0/SMILExtract -C ~/EMOTIVO/openSMILE-2.1.0/config/emobase2010.conf -I ./$1/${file##*/} -O  "./temp/CSV$1_$2_$3_$4/${file##*/}.csv"
	done
fi

echo CSV aggregation ...
$8 find ./temp/CSV$1_$2_$3_$4 -name '*.csv' -exec sh -c './preprocessCode/nth.py $0 $(basename $0)' {} \; > ./temp/DataNoHeader$2Parts$3$1_$4.csv
$8 cut -d',' -f2-  ./temp/DataNoHeader$2Parts$3$1_$4.csv > ./temp/DataNoHeaderNoName$2Parts$3$1_$4.csv
$8 cat ./preprocessCode/header.txt  ./temp/DataNoHeaderNoName$2Parts$3$1_$4.csv > ./temp/Data$2Parts$3Ratio$1_$4.csv

if [ $7 = "TRUE" ]; then
$8	rm -R -f ./temp/CSV$1_$2_$3_$4
fi
$8 	nohup R CMD BATCH --no-timing --no-save --no-restore '--args Data'"$2"'Parts'"$3"'Ratio'"$1_$4"'.csv ./temp/ '"$2"'' ./EMOTIVOEngine/$5 ./temp/RTrail$1_$2_$3_$4_$5.out 
if [ $7 = "TRUE" ]; then
$8 	rm -R -f ./temp/*$1_$4.csv
fi

if [ $6 = "TRUE" ]; then
	nohup R CMD BATCH --no-timing --no-save --no-restore '--args CleanedData'"$2"'Parts'"$3"'Ratio'"$1_$4"'.csv' ./EMOTIVOEngine/EMOTIVO.R  ./temp/RTrailEMOTIVO$1_$2_$3_$4.out 
fi
